import os

import cv2
import numpy as np
import tensorflow as tf


# this module scans directories with files and downloads them in opencv format
def load_data():
    imgs_train_big, imgs_train_small, imgs_test_big, imgs_test_small = [], [], [], []

    root_path = os.getcwd()

    root_train_big = os.path.join(root_path, r'imgs_details/train/big')
    root_train_small = os.path.join(root_path, r'imgs_details/train/small')
    root_test_big = os.path.join(root_path, r'imgs_details/test/big')
    root_test_small = os.path.join(root_path, r'imgs_details/test/small')

    train_big, \
    train_small, \
    test_big, \
    test_small = \
        os.listdir(root_train_big), \
        os.listdir(root_train_small), \
        os.listdir(root_test_big), \
        os.listdir(root_test_small),

    for i in train_big:
        imgs_train_big.append(cv2.imread(os.path.join(root_train_big, i)))
    for i in train_small:
        imgs_train_small.append(cv2.imread(os.path.join(root_train_small, i)))
    for i in test_big:
        imgs_test_big.append(cv2.imread(os.path.join(root_test_big, i)))
    for i in test_small:
        imgs_test_small.append(cv2.imread(os.path.join(root_test_small, i)))
    return imgs_train_big, imgs_train_small, imgs_test_big, imgs_test_small


# this module converts data from opencv format to tensorflow format
def process_data(train_big_cv, train_small_cv, test_big_cv, test_small_cv, width=32, height=32):
    for i, item in enumerate(train_big_cv):
        resized = cv2.resize(item, (width, height))
        rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)
        train_big_cv[i] = tf.expand_dims(rgb_tensor, 0)
    for i, item in enumerate(train_small_cv):
        resized = cv2.resize(item, (width, height))
        rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)
        train_small_cv[i] = tf.expand_dims(rgb_tensor, 0)
    for i, item in enumerate(test_big_cv):
        resized = cv2.resize(item, (width, height))
        rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)
        test_big_cv[i] = tf.expand_dims(rgb_tensor, 0)
    for i, item in enumerate(test_small_cv):
        resized = cv2.resize(item, (width, height))
        rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)
        test_small_cv[i] = tf.expand_dims(rgb_tensor, 0)
    return train_big_cv, train_small_cv, test_big_cv, test_small_cv


def tf_load_data(dir_train, dir_test, img_height=32, img_width=32, batch_size=64):
    train_ds = tf.keras.utils.image_dataset_from_directory(
        dir_train,
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
    val_ds = tf.keras.utils.image_dataset_from_directory(
        dir_test,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)

    AUTOTUNE = tf.data.AUTOTUNE

    train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

    return train_ds, val_ds


def try_to_predict(tensor):
    # Load pretrain model, made from: https://www.tensorflow.org/tutorials/images/cnn
    model = tf.keras.models.load_model('cifar10_model.h5')

    # Create probability model
    probability_model = tf.keras.Sequential([model,
                                             tf.keras.layers.Softmax()])
    # Predict label
    predictions = probability_model.predict(tensor, steps=1)
