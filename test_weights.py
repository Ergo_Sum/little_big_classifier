import os

from model import create_model
from keras.models import load_model
from data import tf_load_data


if __name__ == '__main__':
    model_loaded = load_model('My_model')

    root_path = os.getcwd()
    train_dir = os.path.join(root_path, r'imgs_details/train')
    test_dir = os.path.join(root_path, r'imgs_details/test')
    train_ds, val_ds = tf_load_data(train_dir, test_dir)

    model_loaded.evaluate(val_ds)