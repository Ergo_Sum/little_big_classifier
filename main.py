import os

from data import load_data, process_data, try_to_predict, tf_load_data
from model import create_model
from keras.callbacks import ModelCheckpoint
from keras.models import model_from_json
from sklearn.metrics import roc_auc_score
# train_big_cv, train_small_cv, test_big_cv, test_small_cv = load_data()
#
# train_big_tf, train_small_tf, test_big_tf, test_small_tf = process_data(train_big_cv, train_small_cv,
#                                                                         test_big_cv, test_small_cv)

#try_to_predict(train_big_tf[0])


if __name__ == '__main__':
    root_path = os.getcwd()
    train_dir = os.path.join(root_path, r'imgs_details/train')
    test_dir = os.path.join(root_path, r'imgs_details/test')
    train_ds, val_ds = tf_load_data(train_dir, test_dir)
    Model = create_model((32, 32, 3))
    epochs = 100
    weights_file = "weights.h5"
    checkpoint = ModelCheckpoint(weights_file,  monitor='accuracy', mode='max', save_best_only=True, verbose=1)
    history = Model.fit(train_ds, validation_data=val_ds, epochs=epochs, callbacks=[checkpoint])

    Model.save('My_model')


